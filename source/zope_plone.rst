============
Zope & Plone
============

Python
======

- `A pragmatic talk on Unicode <https://www.youtube.com/watch?v=sgHbC6udIqc>`_

Plone
=====

- `Plone Developer Manual <http://developer.plone.org/>`_


Zope
====

- `A Comprehensive Guide to Zope Component Architecture <http://www.muthukadan.net/docs/zca.html>`_
