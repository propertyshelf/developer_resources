===============
Version Control
===============

Mercurial
=========

- `Mercurial Kick Start <http://mercurial.aragost.com/kick-start/en/>`_
- `HG Tip (Learn Mercurial one bite-sized tip at a time) <http://hgtip.com/>`_

Git
===

- `Pro Git (Expert's Voice in Software Development) <http://git-scm.com/book>`_
- `GitHub Help Center <https://help.github.com>`_
- `Atlassian Git Tutorials <http://www.atlassian.com/git>`_
